module.exports = {
    "staticFileGlobs": [
	'dist/favicon.ico',
	'dist/**.html',
	'dist/**.js',
	'dist/**.css',
	'dist/assets/**/*.png'
    ],
    "stripPrefix": "dist/"
};
