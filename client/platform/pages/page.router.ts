// AUTO GENERATED FROM APP DEF - DO NOT CHANGE MANUALLY
import { Routes } from '@angular/router';

// PAGE LIST

   import { HomePage } from './home/home.page';

   import { CategoryPage } from './category/category.page';

   import { PdpPage } from './pdp/pdp.page';

   import { SearchPage } from './search/search.page';
// PAGE LIST

export const PageComponents = [

    HomePage,
    CategoryPage,
    PdpPage,
    SearchPage
];

export const PageRoutes: Routes = [
    {
	path: '',
	redirectTo: 'home',
	pathMatch: 'full'
    }
,
    {
	path: 'home',
	component: HomePage    }
    ,
    {
	path: 'category',
	component: CategoryPage    }
    ,
    {
	path: 'pdp',
	component: PdpPage    }
    ,
    {
	path: 'search',
	component: SearchPage    }
    
];